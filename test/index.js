/**
 * @author    Herman Nurlygayanov <powersource0160@outlook.com>
 * @copyright Copyright (c) 2020
 * @license   1.0.0
 */

'use strict';

require('babel-register');
require('babel-polyfill');
require('source-map-support').install();
require('dotenv').config({ silent: true });
require('./api');
