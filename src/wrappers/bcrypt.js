/**
 * @author    Herman Nurlygayanov <powersource0160@outlook.com>
 * @copyright Copyright (c) 2020
 * @license   1.0.0
 */

import bcrypt from 'bcrypt';
import Promise from 'bluebird';

/**
 * Promisify `bcrypt`
 * Expose as `default`
 */
export default Promise.promisifyAll(bcrypt);
