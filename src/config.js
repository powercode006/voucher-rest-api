/**
 * @author    Herman Nurlygayanov <powersource0160@outlook.com>
 * @copyright Copyright (c) 2020
 * @license   1.0.0
 */

'use strict';

const config = Object.freeze({
  environment: process.env.NODE_ENV || 'development',
  server: {
    host: '0.0.0.0',
    port: process.env.NODE_PORT || process.env.PORT || 3000
  },
  db: {
    host: process.env.MONGO_HOST || '127.0.0.1',
    port: process.env.MONGO_PORT || 27017,
    name: process.env.MONGO_DB_NAME || 'koa-rest-api',
    nameTest: process.env.MONGO_DB_NAME_TEST || 'koa-rest-api-test'
  }
});

export default config;
