/**
 * @author    Herman Nurlygayanov <powersource0160@outlook.com>
 * @copyright Copyright (c) 2020
 * @license   1.0.0
 */

'use strict';

import Koa from 'koa';
import passport from 'koa-passport';
import Router from 'koa-router';
import importDir from 'import-dir';

import auth from './auth';
import { token } from './auth/oauth2';

const app = new Koa();
app.use(auth(passport));

const router = new Router({
  prefix: '/api'
});

/**
 * @apiDefine TokenError
 * @apiError Unauthorized Invalid JWT token
 *
 * @apiErrorExample {json} Unauthorized-Error:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "status": 401,
 *       "error": "Unauthorized"
 *     }
 */

/**
 * @api {post} /auth Authenticate user
 * @apiVersion 1.0.0
 * @apiName AuthUser
 * @apiGroup Auth
 *
 * @apiParam {String} username      User username.
 * @apiParam {String} password      User password.
 * @apiParam {String} grant_type    OAuth2 grant type specification.
 * @apiParam {String} client_id     client (application) identifier.
 * @apiParam {String} client_secret client (application) secret.
 * @apiExample Example usage:
 * curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -d 'username=powersource0160@outlook.com&password=secret&grant_type=password&client_id=koa-rest-api&client_secret=secret' "http://localhost:3000/api/auth/token"
 */
router.post('/auth/token', token());
const routes = importDir('./routes');

Object.keys(routes).forEach(name => {
  const _route = routes[name];
  router.use(_route.routes());
});
app.use(router.routes());

export default app;
