'use strict';

import mongoose from 'mongoose';
import Router from 'koa-router';
import { authorize } from '../auth/oauth2';
import { objectIdConstraint } from '../../middleware/route-constraints';

const Image = mongoose.model('Image');

const router = new Router({
  prefix: '/images'
});

router.use(authorize());

/**
 * @api {get} /images Get all images
 * @apiVersion 1.0.0
 * @apiPermission user
 * @apiName GetImages
 * @apiGroup Images
 */
router.get('/', async (ctx) => ctx.body = await Image.find({}));

/**
 * @api {get} /images/:id Get image
 * @apiVersion 1.0.0
 * @apiPermission user
 * @apiName GetImage
 * @apiGroup Images
 */
router.get('/:id', objectIdConstraint(),
  async (ctx) => ctx.body = await Image.findById(ctx.params.id));

export default router;
