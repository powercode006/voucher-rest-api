/**
 * @author    Herman c <powersource0160@outlook.com>
 * @copyright Copyright (c) 2020
 * @license   1.0.0
 */

'use strict';

import mongoose from 'mongoose';
import Router from 'koa-router';
import { authorize } from '../auth/oauth2';
import { objectIdConstraint } from '../../middleware/route-constraints';

const User = mongoose.model('User');
const Image = mongoose.model('Image');

const router = new Router({
  prefix: '/users'
});

/**
 * @api {get} /users Get all users
 * @apiVersion 1.0.0
 * @apiPermission user
 * @apiName GetUsers
 * @apiGroup Users
 */
router.get('/', authorize(), async (ctx) => ctx.body = await User.find({}));

/**
 * @api {post} /users Create a new user
 * @apiVersion 1.0.0
 * @apiName CreateUser
 * @apiGroup Users
 *
 * @apiParam {String} firstName  User first name
 * @apiParam {String} lastName   User last name
 * @apiParam {String} username   User name (email format)
 * @apiParam {String} password   User password
 * @apiExample Example usage:
 * curl -H "Content-Type: application/json" -X GET -H "Authorization: Bearer $JWT_TOKEN" http://localhost:3000/api/users
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *      {
 *        "_id":"56d576d19f73e3c32339635c",
 *        "updated_at":"2016-08-05T17:09:44.419Z",
 *        "created_at":"2016-08-05T17:09:44.419Z",
 *        "firstName":"Herman",
 *        "lastName":"Herman",
 *        "username":"powersource0160@outlook.com",
 *        "roles":["user"]
 *       }
 *     ]
 *
 */
router.post('/', async (ctx) => {
  const { firstName, lastName, username, password } = ctx.request.body;

  ctx.body = await User.create({
    firstName,
    lastName,
    username,
    password
  });

  ctx.status = 201;
});

/**
 * @api {get} /users/:id Get user by id
 * @apiPermission user
 * @apiVersion 1.0.0
 * @apiName GetUser
 * @apiGroup Users
 */
router.get('/:id', authorize(), objectIdConstraint(),
  async (ctx) => ctx.body = await User.findById(ctx.params.id));

/**
 * @api {put} /users/:id Update existing user by id
 * @apiPermission user
 * @apiVersion 1.0.0
 * @apiName UpdateUser
 * @apiGroup Users
 */
router.put('/:id', authorize(), objectIdConstraint(), async (ctx) => {
  const { firstName, lastName, username } = ctx.request.body;

  const user = await User.findByIdAndUpdate(ctx.params.id, {
    firstName,
    lastName,
    username
  }, {
    new: true,
    runValidators: true
  });
  if (user) ctx.body = user;
});

/**
 * @api {delete} /users/:id Delete a user
 * @apiPermission user
 * @apiVersion 1.0.0
 * @apiName DeleteUser
 * @apiGroup Users
 */
router.delete('/:id', authorize(), objectIdConstraint(), async (ctx) => {
  const user = await User.findByIdAndRemove(ctx.params.id);
  if (user) ctx.status = 204;
});

/**
 * @api {get} /users/:id/images Get user images
 * @apiPermission user
 * @apiVersion 1.0.0
 * @apiName GetUserImages
 * @apiGroup Users
 */
router.get('/:id/images', authorize(), objectIdConstraint(),
  async (ctx) => ctx.body = await Image.find({ user: ctx.params.id }));

export default router;
