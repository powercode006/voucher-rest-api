/**
 * @author    Herman Nurlygayanov <powersource0160@outlook.com>
 * @copyright Copyright (c) 2020
 * @license   1.0.0
 */

'use strict';

require('babel-register');
require('babel-polyfill');

require('source-map-support').install();

// loads environment variables from a .env file into process.env.
require('dotenv/config');
require('./src');
